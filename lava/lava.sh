#!/bin/bash

# Copyright © 2021 Toshiba corporation
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This script based on https://gitlab.com/cip-project/cip-testing/linux-cip-ci/-/blob/master/submit_tests.sh
# Copyright (C) 2021, Renesas Electronics Europe GmbH, Chris Paterson
# <chris.paterson2@renesas.com>
#
# Script specific dependencies:
# lavacli pwd sed date
#
#
################################################################################

set -e

################################################################################
LAVACLI_ARGS="--uri https://$LAVA_USER:$LAVA_TOKEN@${CIP_LAVA_LAB_SERVER:-lava.ciplatform.org}/RPC2"
LAVA_JOBS_URL="https://${CIP_LAVA_LAB_SERVER:-lava.ciplatform.org}/scheduler/job"
################################################################################

function set_up {
	TMP_DIR="$(mktemp -d)"
}

function clean_up {
	rm -rf "$TMP_DIR"
}

# $1: Device-type to search for
function is_device_online {
	local lavacli_output=$TMP_DIR/lavacli_output

	# Get list of all devices
	lavacli $LAVACLI_ARGS devices list > "$lavacli_output"

	# Count the number of online devices
	local count=$(grep "(${1})" "$lavacli_output" | grep -c "Good")
	echo "There are currently $count \"${1}\" devices online."

	if [ "$count" -gt 0 ]; then
		return 0
	fi
	return 1
}

# $1: Job description yaml file
function submit_job {
	local error=false
	local device=$(grep device_type "$1" | cut -d ":" -f 2 | awk '{$1=$1};1')
	if is_device_online "$device"; then
		# TODO: Add yaml validation
			# Make sure yaml file exists
		if [ -f "$1" ]; then
			echo "Submitting $1 to LAVA master..."
			# Catch error that occurs if invalid yaml file is submitted
			JOB_ID=$(lavacli $LAVACLI_ARGS jobs submit "$1") || error=true

			if [[ "$JOB_ID" != [0-9]* ]]; then
				echo "Something went wrong with job submission. LAVA JOB_IDurned:"
				echo "${JOB_ID}"
			else
				echo "Job submitted successfully as #${JOB_ID}."

				local lavacli_output=$TMP_DIR/lavacli_output
				lavacli $LAVACLI_ARGS jobs show "${JOB_ID}" \
					> "$lavacli_output"

				local status=$(cat "$lavacli_output" \
					| grep "state" \
					| cut -d ":" -f 2 \
					| awk '{$1=$1};1')
				STATUS[${JOB_ID}]=$status

				local health=$(cat "$lavacli_output" \
					| grep "Health" \
					| cut -d ":" -f 2 \
					| awk '{$1=$1};1')
				HEALTH[${JOB_ID}]=$health

				local device_type=$(cat "$lavacli_output" \
					| grep "device-type" \
					| cut -d ":" -f 2 \
					| awk '{$1=$1};1')
				DEVICE_TYPE[${JOB_ID}]=$device_type

				local device=$(cat "$lavacli_output" \
					| grep "device      :" \
					| cut -d ":" -f 2 \
					| awk '{$1=$1};1')
				DEVICE[${JOB_ID}]=$device

				local test=$(cat "$lavacli_output" \
					| grep "description" \
					| rev | cut -d "_" -f 1 | rev)
				TEST[${JOB_ID}]=$test
			fi
		fi
	else
		echo "Refusing to submit test job as there are no suitable devices available."
		error=true
	fi

	if $error; then
		return 1
	fi
	return 0
}

function check_if_all_finished {
	if [ "${STATUS[${JOB_ID}]}" != "Finished" ]; then
		return 1
	fi
	return 0
}

function check_for_test_error {
	if [ "${HEALTH[${JOB_ID}]}" != "Complete" ]; then
		return 0
	fi
	return 1
}

# $1: LAVA job ID to show results for
function get_test_result {
	lavacli $LAVACLI_ARGS results "${JOB_ID}"
}

# $1: Test to print before job summaries
# $2: Set to true to print results for each job
function print_status {
	if [ -z "${1}" ]; then
	# Set default text
		local message="Current job status:"
	else
		local message="${1}"
	fi

	echo "------------------------------"
	echo "${message}"
	echo "------------------------------"
	echo "Job #${JOB_ID}: ${STATUS[${JOB_ID}]}"
	echo "Health: ${HEALTH[${JOB_ID}]}"
	echo "Device Type: ${DEVICE_TYPE[${JOB_ID}]}"
	echo "Device: ${DEVICE[${JOB_ID}]}"
	echo "Test: ${TEST[${JOB_ID}]}"
	echo "URL: ${LAVA_JOBS_URL}/${JOB_ID}"
	if [ -n "${2}" ]; then
		get_test_result "${JOB_ID}"
	fi
		echo " "
}

function print_current_status {
	print_status "Current job status:"
}

function print_summary {
	echo "------------------------------"
	echo "Job Summary"
	echo "------------------------------"
	echo "Job #${JOB_ID} ${STATUS[${JOB_ID}]}. Job health: ${HEALTH[${JOB_ID}]}. URL: ${LAVA_JOBS_URL}/${JOB_ID}"
}

function check_status {
	if [ -n "$TEST_TIMEOUT" ]; then
		# Current time + timeout time
		local end_time=$(date +%s -d "+ $TEST_TIMEOUT min")
	fi

	local error=false

	if [[ "${JOB_ID}" =~ ^[0-9]+$ ]]; then
		print_current_status

		while true; do
			# Get latest status
			if [ "${STATUS[${JOB_ID}]}" != "Finished" ]; then
				local lavacli_output=$TMP_DIR/lavacli_output
				lavacli $LAVACLI_ARGS jobs show "${JOB_ID}" \
					> "$lavacli_output"

				local status=$(cat "$lavacli_output" \
					| grep "state" \
					| cut -d ":" -f 2 \
					| awk '{$1=$1};1')

				local health=$(cat "$lavacli_output" \
					| grep "Health" \
					| cut -d ":" -f 2 \
					| awk '{$1=$1};1')
				HEALTH[${JOB_ID}]=$health

				local device_type=$(cat "$lavacli_output" \
					| grep "device-type" \
					| cut -d ":" -f 2 \
					| awk '{$1=$1};1')
				DEVICE_TYPE[${JOB_ID}]=$device_type

				local device=$(cat "$lavacli_output" \
					| grep "device      :" \
					| cut -d ":" -f 2 \
					| awk '{$1=$1};1')
				DEVICE[${JOB_ID}]=$device

				if [ "${STATUS[$i]}" != "$status" ]; then
					STATUS[${JOB_ID}]=$status

					# Something has changed
					print_current_status
				else
					STATUS[${JOB_ID}]=$status
				fi
			fi

			if check_if_all_finished; then
				break
			fi

			if [ -n "$TEST_TIMEOUT" ]; then
				# Check timeout
				local now=$(date +%s)
				if [ "$now" -ge "$end_time" ]; then
					echo "Timed out waiting for test jobs to complete"
					error=true
					break
				fi
			fi

			# Small wait to avoid spamming the server too hard
			sleep 10
		done

		if check_if_all_finished; then
			# Print job outcome
			print_status "Final job status:" true

			if check_for_test_error; then
				error=true
			fi
		fi
	fi

	if $error; then
		echo "---------------------"
		echo "Errors during testing"
		echo "---------------------"
		print_summary
		clean_up
		return 1
	fi

	echo "-----------------------------------"
	echo "All submitted tests were successful"
	echo "-----------------------------------"
	print_summary
	return 0
}

