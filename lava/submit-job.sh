#!/bin/bash

# Copyright © 2021 Toshiba corporation
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e

usage() {
	echo "Usage: ${0} <device_type> <test>"
	exit 1
}

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

source ${SCRIPT_DIR}/lava.sh

LAVACLI_ARGS="--uri https://$LAVA_USER:$LAVA_TOKEN@${CIP_LAVA_LAB_SERVER:-lava.ciplatform.org}/RPC2"
if [ -z "$SUBMIT_ONLY" ]; then SUBMIT_ONLY=false; fi

if [ -z "${2}" ]; then
	usage
fi
device_type="${1}"
test="${2}"
job_name="cip-core-buster-deby-${device_type}-${test}"

#if [ "$CI_COMMIT_REF_NAME" != "master" ]; then
#	exit 0
#fi

ERROR=false

trap clean_up SIGHUP SIGINT SIGTERM
set_up

echo "Compiling job ${job_name}"
cat lava/jobs/common.yml \
    lava/jobs/device_"${device_type}".yml \
    lava/jobs/test_"${test}".yml \
    > "${job_name}".yml
sed -i "s|DEVICE_TYPE|${device_type}|g" ${job_name}.yml
sed -i "s|JOB_NAME|${job_name}|g" ${job_name}.yml

ERROR=false

# Install lavacli
PATH=$PATH:~/.local/bin
if ! command -v lavacli > /dev/null 2>&1; then
	echo "Installing lavacli"
	sudo apt-get update && sudo apt-get install -y --no-install-recommends lavacli
fi

if ! submit_job ${job_name}.yml; then
	ERROR=true
elif ! $SUBMIT_ONLY; then
	if ! check_status; then
		ERROR=true
	fi
fi

clean_up

if $ERROR; then
	exit 1
fi

exit 0

